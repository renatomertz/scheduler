var app = angular.module('route', ['ngRoute']);

app.config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'pages/scheduling/scheduling.html',
        controller: 'scheduling-controller'
    });
});