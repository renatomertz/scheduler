var app = angular.module('scheduling', ['scheduling']);

app.controller('scheduling-controller',
   function($scope) {
    
    $scope.MINUTE = '1';
    $scope.HOUR = '2';
    $scope.WEEKLY = '3';
    $scope.MONTHLY = '4';

    $scope.horaOpt = "";

    $scope.initClearCollections = function() {
        $scope.chronexpression = "";
        $scope.selectedWeekDays = [];
        $scope.selectedMonths = [];
        $scope.selectedHours = [];
        $scope.allDaysOfWeekSelected = false;
        $scope.allMonthsSelected = false;
    };

    $scope.hours=[{hour: 0}, {hour: 1}, {hour: 2}, {hour: 3}, {hour: 4}, 
                {hour: 5}, {hour: 6}, {hour: 7}, {hour: 8}, {hour: 9}, 
                {hour: 10}, {hour: 11}, {hour: 12}, {hour: 13}, {hour: 14}, 
                {hour: 15}, {hour: 16}, {hour: 17}, {hour: 18}, {hour: 19}, 
                {hour: 20}, {hour: 21}, {hour: 22}, {hour: 23}];

    $scope.days=[{day: '1', value: 1}, {day: '2', value: 2}, {day: '3', value: 3}, {day: '4', value: 4},
                {day: '5', value: 5}, {day: '6', value: 5}, {day: '7', value: 7}, {day: '8', value: 8},
                {day: '9', value: 9}, {day: '10', value: 10}, {day: '11', value: 11}, {day: '12', value: 12}, 
                {day: '13', value: 13}, {day: '14', value: 14}, {day: '15', value: 15}, {day: '16', value: 16}, 
                {day: '17', value: 17}, {day: '18', value: 18}, {day: '19', value: 19}, {day: '20', value: 20}, 
                {day: '21', value: 21}, {day: '22', value: 22}, {day: '23', value: 23}, {day: '24', value: 24},
                {day: '25', value: 25}, {day: '26', value: 26}, {day: '27', value: 27}, {day: '28', value: 28}, 
                {day: '29', value: 29}, {day: '30', value: 30}, {day: '31', value: 31}, {day: 'Último', value: 0}];

    $scope.week=[{day: 'Segunda', value: 'MON', ordinal: 2}, {day: 'Terça', value: 'TUE', ordinal: 3}, 
                {day: 'Quarta', value: 'WED', ordinal: 4}, {day: 'Quinta', value: 'THU', ordinal: 5}, 
                {day: 'Sexta', value: 'FRI', ordinal: 6}, {day: 'Sábado', value: 'SAT', ordinal: 7},
                {day: 'Domingo', value:'SUN', ordinal: 1}];

    $scope.months=[{month: 'Janeiro', value: 'JAN', ordinal: 1}, {month: 'Fevereiro', value: 'FEB', ordinal: 2},
                {month: 'Março', value: 'MAR', ordinal: 3}, {month: 'Abril' , value: 'APR', ordinal: 4},
                {month: 'Maio', value: 'MAY', ordinal: 5}, {month: 'Junho' , value: 'JUN', ordinal: 6},
                {month: 'Julho', value: 'JUL', ordinal: 7},{month: 'Agosto' , value: 'AUG', ordinal: 8},
                {month: 'Setembro', value: 'SEP', ordinal: 9},{month: 'Outubro' , value: 'OCT', ordinal: 10},
                {month: 'Novembro', value: 'NOV', ordinal: 11},{month: 'Dezembro', value: 'DEC', ordinal: 12}];
                

    $scope.selectDayOfWeek = function(dayOfWeek) {
        if($scope.selectedWeekDays.indexOf(dayOfWeek) === -1) {
            $scope.selectedWeekDays.push(dayOfWeek);
        } else {
            $scope.selectedWeekDays.splice(dayOfWeek, 1)            
        }
    };

    $scope.selectAllDaysOfWeek = function() {
        if($scope.allDaysOfWeekSelected) {
            $scope.week.forEach(element => {
                if($scope.selectedWeekDays.indexOf(element) === -1) {
                    $scope.selectedWeekDays.push(element)
                } 
            });
        } else {
            $scope.selectedWeekDays = [];
        }
    };

    $scope.selectHour = function(hour) {
        if($scope.selectedHours.indexOf(hour) === -1) {
            $scope.selectedHours.push(hour);
        } else {
            $scope.selectedHours.splice(hour, 1)            
        }
    };

    $scope.selectMonth = function(month) {
        if($scope.selectedMonths.indexOf(month) === -1) {
            $scope.selectedMonths.push(month);
        } else {
            $scope.selectedMonths.splice(month, 1)            
        }
    };

    $scope.selectAllMonths = function() {
        if($scope.allMonthsSelected) {
            $scope.months.forEach(element => {
                if($scope.selectedMonths.indexOf(element.value) === -1) {
                    $scope.selectedMonths.push(element.value)
                } 
            });
        } else {
            $scope.selectedMonths = [];
        }
    };

    $scope.extractChron = function() {
        $scope.chronexpression = "";
        var hour, minute, second;
        var day, month, year;
        if($scope.date != '') {
            day = 17;
            month = 2018;
            year = 2019;
        }

        if($scope.hour != '') {
            hour = 17;
            minute = 15;
            second = 1;
        }

        switch ($scope.period) {
            case $scope.MINUTE:
                $scope.chronexpression = "0 0/" + $scope.selectedMinute + " * 1/1 * ? *";
                break;
            case $scope.HOUR:
                if($scope.horaOpt === 'H') {
                    $scope.chronexpression = "0 0 0/" + $scope.hour + " ? * * *";
                } else if($scope.horaOpt === 'S') {
                    var sortedHours = $scope.selectedHours.sort(function(a, b){return a.hour - b.hour});
                    var joinHours = sortedHours.map(element =>{return element.hour;}).join(',');
                    $scope.chronexpression = "0 0 " + joinHours + " ? * * *";
                }
            break;
            case $scope.WEEKLY:
                var sortedWeek = $scope.selectedWeekDays.sort(function(a, b){return a.ordinal - b.ordinal});
                var joinWeek = sortedWeek.map(element =>{return element.value;}).join(',');
                $scope.chronexpression = "0 0 0 ? * " + joinWeek + " *";
            break;
            case $scope.MONTHLY:
                $scope.chronexpression = "UNDER CONSTRUCTION";
            break;
            default:
                $scope.chronexpression = "NOT IMPLEMENTED";
            break;
        }
        console.log($scope.chronexpression);
        return $scope.chronexpression;
    };
    
    $scope.initClearCollections();

});